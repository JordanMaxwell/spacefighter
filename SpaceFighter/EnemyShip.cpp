
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	// Check if the spawn delay has expired
	if (m_delaySeconds > 0)
	{
		// If the spawn delay has not expired subtract the elapsed clock time
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		// Check if the final delay seconds is equal to or less then zero.
		// If this check passes activate the object display it on screen.
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	// Verify the object is active
	if (IsActive())
	{
		// Check the duration the ship has been active and if it is no longer on scree.
		// If either check fails deactive the object removing it from render.
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	// Perform ship updates for this instance
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}