
#include "PlayerShip.h"


void PlayerShip::LoadContent(ResourceManager *pResourceManager)
{
	ConfineToScreen();
	SetResponsiveness(0.1);

	m_pTexture = pResourceManager->Load<Texture>("Textures\\PlayerShip.png");

	SetPosition(Game::GetScreenCenter() + Vector2::UNIT_Y * 300);
}


 void PlayerShip::Update(const GameTime *pGameTime)
 {
	 // Get the velocity for the direction that the player is trying to go.
	 Vector2 targetVelocity = m_desiredDirection * GetSpeed() * pGameTime->GetTimeElapsed();
	 // We can't go from 0-100 mph instantly! This line interpolates the velocity for us.
	 m_velocity = Vector2::Lerp(m_velocity, targetVelocity, GetResponsiveness());
	 // Move that direction
	 TranslatePosition(m_velocity);

	 if (m_isConfinedToScreen)
	 {
		 const int PADDING = 4; // keep the ship 4 pixels from the edge of the screen
		 const int TOP = PADDING;
		 const int LEFT = PADDING;
		 const int RIGHT = Game::GetScreenWidth() - PADDING;
		 const int BOTTOM = Game::GetScreenHeight() - PADDING;

		 Vector2 *pPosition = &GetPosition(); // current position (middle of the ship)
		 if (pPosition->X - GetHalfDimensions().X < LEFT) // are we past the left edge?
		 {
			 // move the ship to the left edge of the screen (keep Y the same)
			 SetPosition(LEFT + GetHalfDimensions().X, pPosition->Y);
			 m_velocity.X = 0; // remove any velocity that could potentially
							   // keep the ship pinned against the edge
		 }
		 if (pPosition->X + GetHalfDimensions().X > RIGHT) // right edge?
		 {
			 SetPosition(RIGHT - GetHalfDimensions().X, pPosition->Y);
			 m_velocity.X = 0;
		 }
		 if (pPosition->Y - GetHalfDimensions().Y < TOP) // top edge?
		 {
			 SetPosition(pPosition->X, TOP + GetHalfDimensions().Y);
			 m_velocity.Y = 0;
		 }
		 if (pPosition->Y + GetHalfDimensions().Y > BOTTOM) // bottom edge?
		 {
			 SetPosition(pPosition->X, BOTTOM - GetHalfDimensions().Y);
			 m_velocity.Y = 0;
		 }
	 }

	 // do any updates that a normal ship would do.
	 // (fire weapons, collide with objects, etc.)
	 Ship::Update(pGameTime);
 }

 void PlayerShip::Draw(SpriteBatch *pSpriteBatch)
 {
	 if (IsActive())
	 {
		 pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter());
	 }
 }

 void PlayerShip::HandleInput(const InputState *pInput)
 {
	 // Verify the PlayerShip instance is an active game object.
	 // If the ship is not active ignore this input handle call.
	 if (IsActive())
	 {
		 // Use the user key inputs to calculate the movement direction for
		 // the ship object
		 Vector2 direction = Vector2::ZERO;
		 if (pInput->IsKeyDown(Key::DOWN)) direction.Y++;
		 if (pInput->IsKeyDown(Key::UP)) direction.Y--;
		 if (pInput->IsKeyDown(Key::RIGHT)) direction.X++;
		 if (pInput->IsKeyDown(Key::LEFT)) direction.X--;

		 // Normalize the calculated direction vector
		 if (direction.X != 0 && direction.Y != 0)
		 {
			 direction *= Math::NORMALIZE_PI_OVER4;
		 }

		 //TriggerType type = TriggerType::NONE;
		 //if (pInput->IsKeyDown(Key::F)) type |= TriggerType::PRIMARY;
		 //if (pInput->IsKeyDown(Key::D)) type |= TriggerType::SECONDARY;
		 //if (pInput->IsKeyDown(Key::S)) type |= TriggerType::SPECIAL;

		 // Check if a gamepad is connected. If a gamepad is found
		 // use its input data in place of the keyboard provided data
		 GamePadState *pState = &pInput->GetGamePadState(0);
		 if (pState->IsConnected)
		 {
			 // Calculate the movement direction vector to replace the 
			 // keyboard provided direction.
			 Vector2 thumbstick = pState->Thumbsticks.Left;

			 // Verify the thumbstick is outside the deadzone. If within deadzone range
			 // then nullify the input.
			 if (thumbstick.LengthSquared() < 0.08f) thumbstick = Vector2::ZERO;
			 direction = thumbstick;

			 //type = TriggerType::NONE;
			 //if (pState->Triggers.Right > 0.5f) type |= TriggerType::PRIMARY;
			 //if (pState->Triggers.Left > 0.5f) type |= TriggerType::SECONDARY;
			 //if (pState->IsButtonDown(Button::Y)) type |= TriggerType::SPECIAL;
		 }

		 // Set the ships desired directional vector
		 SetDesiredDirection(direction);
		 //if (type != TriggerType::NONE) FireWeapons(type);

		 // Check if the space button is being held. If the button is held
		 // fire the requested weapon type
		 if (pInput->IsKeyDown(Key::SPACE)) FireWeapons(TriggerType::PRIMARY);
	 }
 }


 void PlayerShip::Initialize(Level *pLevel, Vector2 &startPosition)
 {
	 SetPosition(startPosition);
 }


 Vector2 PlayerShip::GetHalfDimensions() const
 {
	 return m_pTexture->GetCenter();
 }

 void PlayerShip::SetResponsiveness(const float responsiveness)
 {
	 m_responsiveness = Math::Clamp(0, 1, responsiveness);
 }
