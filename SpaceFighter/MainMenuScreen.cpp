
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

// Callback Functions
void OnStartGameSelect(MenuScreen *pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnQuitSelect(MenuScreen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetQuitFlag();
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}

MainMenuScreen::MainMenuScreen()
{
	m_pLogo = nullptr;
	m_pBackground = nullptr;

	SetRemoveCallback(OnScreenRemove);

	SetTransitionInTime(2.0f);
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	m_pLogo = pResourceManager->Load<Texture>("Textures\\Logo.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 220;

	// Background
	m_pBackground = pResourceManager->Load<Texture>("Textures\\Backgrounds\\black.png");

	// Create the menu items
	const int COUNT = 2;
	MenuItem *pItem;
	Font::SetLoadSize(20, true);
	Font *pFont = pResourceManager->Load<Font>("Fonts\\arial.ttf");

	SetDisplayCount(COUNT);

	enum Items { START_GAME, QUIT };
	std::string text[COUNT] = { "Start Game", "Quit" };

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(100, 100 + 50 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::White);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}

	GetMenuItem(START_GAME)->SetSelectCallback(OnStartGameSelect);
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect);
}

void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::Blue);
		else pItem->SetColor(Color::White);
	}

	MenuScreen::Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	// Start sprite batch draw
	pSpriteBatch->Begin();

	// Draw scrolling space background
	int drawWidth = Game::GetScreenWidth() / m_pBackground->GetWidth();
	int drawHeight = (Game::GetScreenHeight() / m_pBackground->GetHeight()) * 2;
	for (int x = 0; x <= drawWidth; x++) {
		for (int y = -Game::GetScreenHeight(); y <= drawHeight; y++) {
			Vector2 drawPos = Vector2(
				x * m_pBackground->GetWidth(),
				y * m_pBackground->GetHeight() + m_backgroundOffset);
			pSpriteBatch->Draw(m_pBackground, drawPos, Color::White * GetAlpha(), m_pBackground->GetCenter());
		}
	}

	// Draw logo and end batch
	pSpriteBatch->Draw(m_pLogo, m_texturePosition, Color::White * GetAlpha(), m_pLogo->GetCenter());
	pSpriteBatch->End();

	// Change background offset
	m_backgroundOffset += 50;
	if (m_backgroundOffset >= Game::GetScreenHeight()) {
		m_backgroundOffset = 0;
	}

	MenuScreen::Draw(pSpriteBatch);
}
